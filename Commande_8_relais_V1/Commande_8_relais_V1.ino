// test de fonctionnement de 8 sortie sur Wemos D1 V1 sans problème d'interference au boot

const int chOut[6] = {5, 4, 14, 12, 13, 15};
const int chIn[3] = {0A, 0, 2};

void setup() {
  Serial.begin(9600);

  for (int i = 0; i <= 5; i++) {
    pinMode(chOut[i], OUTPUT);
  }
  for (int i = 0; i <= 1; i++) {
    pinMode(chIn[i], INPUT_PULLUP);
  }

  //  Serial.println("Pause de 2 secondes");
  //  delay(2000);
  //  Serial.println("Fin seput");

}

void loop() {

  for (int i = 0; i <= 5; i++) {
    digitalWrite(chOut[i], HIGH);

    delay(500);
  }

  for (int i = 0; i <= 5; i++) {
    digitalWrite(chOut[i], LOW);

    delay(500);
  }
  Serial.println("Bonjour");
}

