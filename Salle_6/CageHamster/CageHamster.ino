/*---------------------------------------------------------------------------------------------
Salle 6

Cage Hamster


 je reçois un messgage OSC de type (pas un paquet)
 /cageH X2

 et X2 action 1
  Routeur-6
  9876543210

  IP 192.168.6.10

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>

#include <RBD_Timer.h> // https://github.com/alextaujenis/RBD_Timer

RBD::Timer timer(10000);

//String ssid = "gildas";
//String password = "20070512";

String ssid = "routeur-6";       //   Nom du reseau
String password = "9876543210";

// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;
const unsigned int localPort = 9999;        // local port to listen for UDP packets (here's where we send the packets)

OSCErrorCode error;

// Variables globales

const int pinMotor = 5; //pin de commande du moteur
const int pinEn = 4; // activation moteur


void setup() {

  Serial.begin(115200);

  pinMode(pinMotor,OUTPUT);
  pinMode(pinEn, OUTPUT);

  digitalWrite(pinEn, LOW);

  IPAddress ip(192, 168, 6, 60);        //IP carte cage à Hamster
  IPAddress subnet(255, 255, 0, 0);
   IPAddress gt(192, 168, 43, 1);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());

  Serial.println("fin de setup");

}



void loop() {
  if(timer.onExpired()) {
    // analogWrite(pinMotor, 1023);
     digitalWrite(pinEn, LOW),
    // code only runs once per event
    Serial.println("Timer Expired");
    
  }
  OSCMessage msg;                       // OSCBundle bundle;

  int size = Udp.parsePacket();

  if (size > 0) {
    while (size--) {
      msg.fill (Udp.read());            //bundle.fill(Udp.read());
    }
    if (!msg.hasError()) {              //  if (!bundle.hasError()) {
      Serial.println("message ");
      msg.dispatch("/cageH", Play);

    } else {
      error = msg.getError();           //  error = bundle.getError();
      Serial.println("error: ");
      //      Serial.println(error);
    }
  }
  delay(10);
}



