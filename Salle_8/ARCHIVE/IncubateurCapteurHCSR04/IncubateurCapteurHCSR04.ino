
/*---------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <HCSR04.h>

UltraSonicDistanceSensor distanceSensor(5, 16);

//char ssid[] = "gildas";          // your network SSID (name)
//char pass[] = "20070512";                    // your network password

//String ssid = "gildas"; // static IP
//String password = "20070512";//

String ssid = "routeur-7"; // static IP
String password = "9876543210";//

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 7, 10);     // remote IP of your computer
//const IPAddress outIp2(192, 168, 43, 100);     // remote IP of your computer
const unsigned int outPort = 8888;          // remote port to receive OSC
const unsigned int localPort = 9999;        // local port to listen for OSC packets (actually not used for sending)

void setup() {
  Serial.begin(115200);

  // Static IP
  IPAddress ip(192, 168, 7, 202);
  IPAddress subnet(255, 255, 0, 0);
  IPAddress gt(192, 168, 1, 1);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("Local IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();
  Serial.println("outPort");
  Serial.println(outPort);


}

void loop() {

float distance = distanceSensor.measureDistanceCm();
  OSCMessage msg("Inc1");
  Serial.println(distance);
msg.add(distance);

  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();
  
  msg.empty();
  delay(10);
}

