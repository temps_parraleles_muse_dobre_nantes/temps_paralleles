/*---------------------------------------------------------------------------------------------
  Salle 6


  je reçois un messgage OSC de type (pas un paquet)
  salleX1 X2

  ou X1 est le N° de la salle
  et X2 la seqence a jouer

  Routeur-6
  9876543210

  IP 192.168.6.10

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>
#include <HCSR04.h>
#include <RBD_Timer.h> // https://github.com/alextaujenis/RBD_Timer


// declaration capteur

UltraSonicDistanceSensor distanceSensor(5, 16);

// DECLARATION TIMER CAPTEUR

RBD::Timer timerCapteur;

//String ssid = "gildas";
//String password = "20070512";

String ssid = "routeur-7";       //   Nom du reseau
String password = "9876543210";

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 7, 10);     // remote IP of your computer
//const IPAddress outIp2(192, 168, 43, 100);     // remote IP of your computer
const unsigned int outPort = 8888;          // remote port to receive OSC
const unsigned int localPort = 9999;        // local port to listen for OSC packets (actually not used for sending)

OSCErrorCode error;

// Variables globales

const int ch[4] = {4, 14, 12, 13}; //N° des pin de sortie relais WeMos D1 retired

unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 3000;           // interval at which to blink (milliseconds)

//*****************************************//
//************  SET UP     ****************//
//*****************************************//

void setup() {

  Serial.begin(115200);

  for (int i = 0; i <= 3; i++) {// configuratin PIN OUT pour commande lecteur ID_AL
    pinMode(ch[i], OUTPUT);
  }

  timerCapteur.setTimeout(200);
  timerCapteur.restart();

  IPAddress ip(192, 168, 7, 201);        //IP carte Inc
  IPAddress subnet(255, 255, 0, 0);
  IPAddress gt(192, 168, 43, 1);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());

  Arreter ();

  Serial.println("fin de setup");

}

//*****************************************//
//**************   Void loop   ************//
//*****************************************//

void loop() {

  //*****************************************//
  //**** Envoi OSC  **************************//


  OSCMessage msg;                       // OSCBundle bundle;

  int size = Udp.parsePacket();

  if (size > 0) {
    while (size--) {
      msg.fill (Udp.read());            //bundle.fill(Udp.read());
    }
    if (!msg.hasError()) {              //  if (!bundle.hasError()) {
      Serial.println("message ");
      msg.dispatch("/Inc1", Play);

    } else {
      error = msg.getError();           //  error = bundle.getError();
      Serial.println("error: ");
      //      Serial.println(error);
    }
  }

  //*****************************************//
  //*****************************************//

  if (timerCapteur.onRestart()) {
    float distance = distanceSensor.measureDistanceCm();
    OSCMessage msgSend("Inc1");
    Serial.println(distance);
    msgSend.add(distance);

    Udp.beginPacket(outIp, outPort);
    msgSend.send(Udp);
    Udp.endPacket();
    msgSend.empty();
  }

}
