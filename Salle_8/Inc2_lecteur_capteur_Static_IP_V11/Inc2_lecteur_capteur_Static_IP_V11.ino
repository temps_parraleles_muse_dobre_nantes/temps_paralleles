/*---------------------------------------------------------------------------------------------
  Inccubateur

  V11 integration relais lecteure video 1

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>
#include <HCSR04.h>
#include <RBD_Timer.h> // https://github.com/alextaujenis/RBD_Timer
#include <RBD_Button.h> // https://github.com/alextaujenis/RBD_Button

//*****************************************//
//*******    Variable et Objets   *********//
//*****************************************//

const int ch[4] = {4, 14, 12, 13}; //N° des pin de sortie relais WeMos D1 retired

//*********   declaration capteur

UltraSonicDistanceSensor distanceSensor(5, 16);

//**********  declaration Pin pour info contact lecture

RBD::Button lectureOn(0);

//**********  DECLARATION TIMER CAPTEUR

RBD::Timer timerCapteur;

//*****************************************//
//************    Wifi     ****************//
//*****************************************//

String ssid = "routeur-7";       //   Nom du reseau
String password = "9876543210";

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 7, 10);     // remote IP of your computer
//const IPAddress outIp2(192, 168, 43, 100);     // remote IP of your computer
const unsigned int outPort = 8888;          // remote port to receive OSC
const unsigned int localPort = 9999;        // local port to listen for OSC packets (actually not used for sending)

OSCErrorCode error;



//*****************************************//
//************  SET UP     ****************//
//*****************************************//

void setup() {

  Serial.begin(115200);
  //*************    init Pin commande lecteur

  for (int i = 0; i <= 3; i++) {// configuratin PIN OUT pour commande lecteur ID_AL
    pinMode(ch[i], OUTPUT);
  }

  //*************    init timer Capteur

  timerCapteur.setTimeout(200);
  timerCapteur.restart();

  IPAddress ip(192, 168, 7, 202);        //IP carte Inc
  IPAddress subnet(255, 255, 0, 0);
  IPAddress gt(192, 168, 43, 1);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());

  Arreter ();

  Serial.println("fin de setup");

}

//*****************************************//
//**************   Void loop   ************//
//*****************************************//

void loop() {

  //*****************************************//
  //**** Envoi OSC  **************************//


  OSCMessage msg;                       // OSCBundle bundle;

  int size = Udp.parsePacket();

  if (size > 0) {
    while (size--) {
      msg.fill (Udp.read());            //bundle.fill(Udp.read());
    }
    if (!msg.hasError()) {              //  if (!bundle.hasError()) {
      Serial.println("message ");
      msg.dispatch("/Inc2", Play);

    } else {
      error = msg.getError();           //  error = bundle.getError();
      Serial.println("error: ");
      //      Serial.println(error);
    }
  }

  //*****************************************//
  //*****************************************//

  if (timerCapteur.onRestart()) {
    float distance = distanceSensor.measureDistanceCm();
    OSCMessage msgSend("cap2");
    Serial.println(distance);
    msgSend.add(distance);

    Udp.beginPacket(outIp, outPort);
    msgSend.send(Udp);
    Udp.endPacket();
    msgSend.empty();
  }
  if (lectureOn.onPressed()) {
    Serial.println("Button Pressed");
    OSCMessage msglect("lec2");
    msglect.add(1);
    Udp.beginPacket(outIp, outPort);
    msglect.send(Udp);
    Udp.endPacket();
    msglect.empty();
  }

  if (lectureOn.onReleased()) {
    Serial.println("Button Released");
    OSCMessage msglect("lec2");
    msglect.add(0);
    Udp.beginPacket(outIp, outPort);
    msglect.send(Udp);
    Udp.endPacket();
    msglect.empty();

  }
}
