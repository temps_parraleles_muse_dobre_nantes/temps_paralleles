/*---------------------------------------------------------------------------------------------
  Telephone decroche et message

  --------------------------------------------------------------------------------------------- */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>
#include <HCSR04.h>
#include <RBD_Timer.h> // https://github.com/alextaujenis/RBD_Timer
#include <RBD_Button.h> // https://github.com/alextaujenis/RBD_Button

////*****************************************//
////*******    Variable et Objets   *********//
////*****************************************//
//
//const int ch[4] = {5, 4, 14, 12}; //N° des pin de sortie relais WeMos D1 retired


//**********  declaration Pin pour info contact lecture

RBD::Button decTel1(5, false);
RBD::Button decTel2(4, false);
RBD::Button decTel3(14, false);

//**********  DECLARATION TIMER CAPTEUR

RBD::Timer timerDecroche;

//*****************************************//
//************    Wifi     ****************//
//*****************************************//

String ssid = "routeur-7";       //   Nom du reseau
String password = "9876543210";

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 7, 10);     // remote IP of your computer
//const IPAddress outIp2(192, 168, 43, 100);     // remote IP of your computer
const unsigned int outPort = 8888;          // remote port to receive OSC
const unsigned int localPort = 9999;        // local port to listen for OSC packets (actually not used for sending)

OSCErrorCode error;



//*****************************************//
//************  SET UP     ****************//
//*****************************************//

void setup() {

  Serial.begin(115200);

  //*************    init timer Capteur

  timerDecroche.setTimeout(50);
  timerDecroche.restart();

  IPAddress ip(192, 168, 7, 206);        //IP carte Inc
  IPAddress subnet(255, 255, 0, 0);
  IPAddress gt(192, 168, 43, 1);
  char charBuf[50];
  char charBuf2[50];
  ssid.toCharArray(charBuf, 50);
  password.toCharArray(charBuf2, 50);
  WiFi.config(ip, gt, subnet);
  WiFi.begin(charBuf , charBuf2);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  //  Serial.println("Starting UDP");
  Udp.begin(localPort);
  //  Serial.print("Local port: ");
  //  Serial.println(Udp.localPort());


  //  Serial.println("fin de setup");
  OSCMessage decOk("Carte_Dec_OK");
  Udp.beginPacket(outIp, outPort);
  decOk.send(Udp);
  Udp.endPacket();
  decOk.empty();

}

//*****************************************//
//**************   Void loop   ************//
//*****************************************//

void loop() {

  //*****************************************//
  //***************Tel 1960******************//

  if (decTel1.onPressed()) {
    //    Serial.println("Tel1 décroché");
    OSCMessage msgdec1("dec1");
    msgdec1.add(1);
    Udp.beginPacket(outIp, outPort);
    msgdec1.send(Udp);
    Udp.endPacket();
    msgdec1.empty();
  }

  if (decTel1.onReleased()) {
    //    Serial.println("Tel1 racroché");
    OSCMessage msgdec1("dec1");
    msgdec1.add(0);
    Udp.beginPacket(outIp, outPort);
    msgdec1.send(Udp);
    Udp.endPacket();
    msgdec1.empty();
  }

  //*****************************************//
  //***************Tel 1986******************//

  if (decTel2.onPressed()) {
    //    Serial.println("Tel2 décroche");
    OSCMessage msgdec2("dec2");
    msgdec2.add(1);
    Udp.beginPacket(outIp, outPort);
    msgdec2.send(Udp);
    Udp.endPacket();
    msgdec2.empty();
  }

  if (decTel2.onReleased()) {
    //    Serial.println("Tel2 racroché");
    OSCMessage msgdec2("dec2");
    msgdec2.add(0);
    Udp.beginPacket(outIp, outPort);
    msgdec2.send(Udp);
    Udp.endPacket();
    msgdec2.empty();
  }

  //*****************************************//
  //***************Tel 2018******************//

  if (decTel3.onPressed()) {
    //    Serial.println("Tel3 décroche");
    OSCMessage msgdec3("dec3");
    msgdec3.add(1);
    Udp.beginPacket(outIp, outPort);
    msgdec3.send(Udp);
    Udp.endPacket();
    msgdec3.empty();
  }

  if (decTel3.onReleased()) {
    //    Serial.println("Tel3 racroché");
    OSCMessage msgdec3("dec3");
    msgdec3.add(0);
    Udp.beginPacket(outIp, outPort);
    msgdec3.send(Udp);
    Udp.endPacket();
    msgdec3.empty();
  }

}
